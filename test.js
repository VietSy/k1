const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors'); // Device switching
const iPhone = devices['iPhone 6']; //Test on Iphone 6

const url = 'http://wp13.test20008.com/contact/';

const Device = async device => {
	const browser = await puppeteer.launch({headless: false});
	const page = await browser.newPage();
	await page.emulate(iPhone);
	await page.goto(url);
	await page.screenshot({path: 'screenshot_mobi.png', fullPage: true});

	await browser.close();
}

const Type = async username => {
	const browser = await puppeteer.launch({headless: false});
	const page = await browser.newPage();
	page.setViewport({ width: 1920, height:1080 });
	await page.goto(url);
	await page.type('input[name="name"]', 'Viet Sy');
	await page.type('input[name="zipcode"]', '71400');
	await page.type('input[name="address"]', 'GoVap, HoChiMinh, VietNam');
	await page.type('textarea[name="messenger"]', 'Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy');
	await page.screenshot({path: 'screenshot_type.png', fullPage: true});

	await browser.close();
}

const Validation = async username => {
	const browser = await puppeteer.launch({headless: false});
	const page = await browser.newPage();
	page.setViewport({ width: 1920, height:1080 });
	await page.goto(url);
	await page.click('input[type="submit"]');  
	await page.screenshot({path: 'screenshot_validation.png', fullPage: true});

	await browser.close();
}

const Confirm = async username => {
	const browser = await puppeteer.launch({headless: false});
	const page = await browser.newPage();
	page.setViewport({ width: 1920, height:1080 });
	await page.goto(url);
	await page.type('input[name="name"]', 'Viet Sy');
	await page.type('input[name="zipcode"]', '71400');
	await page.type('input[name="address"]', 'GoVap, HoChiMinh, VietNam');
	await page.type('textarea[name="messenger"]', 'Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy');
	await page.click('input[type="submit"]');  
	await page.screenshot({path: 'screenshot_confirm.png', fullPage: true});

	await browser.close();
}

const Error = async username => {
	const browser = await puppeteer.launch();
	const page = await browser.newPage();
	page.on('console', msg => console.log('PAGE LOG:', msg.text));
	page.on('pageerror', error => {
		console.log(error.message);
	});
	await page.goto(url);
	await page.waitFor(5000);
	
	await browser.close();
}

switch (process.argv[2]) {
	case 'mobi':
		Device(process.argv[3])
		break
	case 'type':
		Type(process.argv[3])
		break
	case 'validation':
		Validation(process.argv[3])
		break	
	case 'confirm':
		Confirm(process.argv[3])
		break
	case 'err':
		Error(process.argv[3])
		break
	default:
		console.log('Wrong syntax!!!')
}