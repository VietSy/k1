const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors'); // Device switching
const iPhone = devices['iPhone 6']; //Test on Iphone 6

var url = 'http://wp13.test20008.com/contact/';
var txt1 = 'Viet Sy',
	txt2 = '71400',
	txt3 = 'GoVap, HoChiMinh, VietNam',
	txt4 = 'Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy Dummy';

class Device{
	constructor(){
		(async () => {
			const browser = await puppeteer.launch({headless: false});
			const page = await browser.newPage();
			await page.emulate(iPhone);
			await page.goto(url);
			await page.screenshot({path: 'screenshot_mobi.png', fullPage: true});

			await browser.close();
		})();
	}
}

class Type{
    constructor(){
        (async () => {
			const browser = await puppeteer.launch({headless: false});
			const page = await browser.newPage();
			page.setViewport({ width: 1920, height:1080 });
			await page.goto(url);
			await page.type('input[name="name"]', txt1);
			await page.type('input[name="zipcode"]', txt2);
			await page.type('input[name="address"]', txt3);
			await page.type('textarea[name="messenger"]', txt4);
			await page.screenshot({path: 'screenshot_type.png', fullPage: true});

			await browser.close();
		})();
    }
}

class Validation{
	constructor(){
		(async () => {
			const browser = await puppeteer.launch({headless: false});
			const page = await browser.newPage();
			page.setViewport({ width: 1920, height:1080 });
			await page.goto(url);
			await page.click('input[type="submit"]');  
			await page.screenshot({path: 'screenshot_validation.png', fullPage: true});

			await browser.close();
		})();
	}
}

class Confirm{
	constructor(){
		(async () => {
			const browser = await puppeteer.launch({headless: false});
			const page = await browser.newPage();
			page.setViewport({ width: 1920, height:1080 });
			await page.goto(url);
			await page.type('input[name="name"]', txt1);
			await page.type('input[name="zipcode"]', txt2);
			await page.type('input[name="address"]', txt3);
			await page.type('textarea[name="messenger"]', txt4);
			await page.click('input[type="submit"]');  
			await page.screenshot({path: 'screenshot_confirm.png', fullPage: true});

			await browser.close();
		})();
	}
}

class Err{
	constructor(){
		(async () => {
			const browser = await puppeteer.launch();
			const page = await browser.newPage();
			page.on('console', msg => console.log('PAGE LOG:', msg.text));
			page.on('pageerror', error => {
				console.log(error.message);
			});
			await page.goto(url);
			await page.waitFor(5000);

			await browser.close();
		})();
	}
}

module.exports = {
    Device,
    Type,
    Validation,
    Confirm,
    Err
};